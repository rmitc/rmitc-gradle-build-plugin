package com.rmitc.gradle.plugin.extension

class RmitcPluginExtension {

    String bintrayRepoId

    String description

    String artifactId

    String vcsUrl

    String[] labels

}
