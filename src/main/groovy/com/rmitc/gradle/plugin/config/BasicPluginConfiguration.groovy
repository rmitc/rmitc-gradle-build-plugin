package com.rmitc.gradle.plugin.config

import com.rmitc.gradle.plugin.extension.RmitcPluginExtension
import org.gradle.api.Plugin
import org.gradle.api.Project

class BasicPluginConfiguration implements PluginConfiguration<Project> {

    static final String GROUP_NAME = 'rmitc tasks'

    @Override
    void configure(Project project) {
        project.pluginManager.apply(BasicPlugin)
        project.pluginManager.apply(BasicHelperPlugin)
    }

    static class BasicPlugin implements Plugin<Project> {

        @Override
        void apply(Project project) {
            if (!project.group) {
                project.group = 'com.rmitc'
            }

            project.task('projectInfo') {
                group GROUP_NAME
                doLast {
                    println("Group  : ${project.group}")
                    println("Name   : ${project.name}")
                    println("Version: ${project.version}")
                }
            }
        }

    }

    static class BasicHelperPlugin implements Plugin<Project> {

        @Override
        void apply(Project project) {
            project.extensions.create('rmitc', RmitcPluginExtension)
        }

    }

}
