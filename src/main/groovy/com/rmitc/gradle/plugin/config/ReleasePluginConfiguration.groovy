package com.rmitc.gradle.plugin.config

import net.researchgate.release.ReleasePlugin
import org.gradle.api.Project

class ReleasePluginConfiguration implements PluginConfiguration<Project> {

    @Override
    void configure(Project project) {
        project.pluginManager.apply(ReleasePlugin)
    }

}
