package com.rmitc.gradle.plugin.config

import com.rmitc.gradle.plugin.extension.RmitcPluginExtension
import org.gradle.api.Project
import org.gradle.api.XmlProvider
import org.gradle.api.publish.PublishingExtension
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.publish.maven.plugins.MavenPublishPlugin

class MavenPublishPluginConfiguration implements PluginConfiguration<Project> {

    @Override
    void configure(Project project) {
        project.pluginManager.apply(MavenPublishPlugin)
        def rmitcExtension = project.extensions.findByType(RmitcPluginExtension)
        def publishingExtension = project.extensions.findByType(PublishingExtension)
        publishingExtension.with {
            publications {
                RMITCLibrary(MavenPublication) {
                    from project.components.java
                    artifact project.tasks.findByName('sourcesJar')
                    artifact project.tasks.findByName('javadocJar')
                    groupId project.group
                    artifactId project.name
                    version project.version
                    pom.withXml { XmlProvider xmlProvider ->
                        def root = xmlProvider.asNode()
                        root.appendNode('description', rmitcExtension.description)
                        root.appendNode('name', project.name)
                        root.appendNode('url', rmitcExtension.vcsUrl)
                    }
                }
            }
        }
    }

}
