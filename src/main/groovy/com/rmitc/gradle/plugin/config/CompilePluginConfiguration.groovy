package com.rmitc.gradle.plugin.config


import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.plugins.GroovyPlugin
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.plugins.JavaPluginConvention
import org.gradle.api.tasks.bundling.Jar
import org.gradle.api.tasks.javadoc.Javadoc
import org.gradle.api.tasks.testing.Test

import static org.gradle.api.tasks.testing.logging.TestLogEvent.*

class CompilePluginConfiguration implements PluginConfiguration<Project> {

    @Override
    void configure(Project project) {
        project.pluginManager.apply(JavaPlugin)
        project.pluginManager.apply(GroovyPlugin)
        configureTestTask(project)
        configureSourceJarTask(project)
        configureJavadocJarTask(project)
    }

    private static void configureTestTask(Project project) {
        project.tasks.withType(Test).configureEach(new Action<Test>() {

            @Override
            void execute(Test test) {
                test.testLogging.events = [PASSED, FAILED, SKIPPED]
            }

        })
    }

    private static void configureSourceJarTask(Project project) {
        def javaPluginConvention = project.convention.getPlugin(JavaPluginConvention) as JavaPluginConvention
        project.tasks.create('sourcesJar', Jar) {
            dependsOn: 'classes'
            archiveClassifier.set('sources')
            from javaPluginConvention.sourceSets.main.allSource
        }
    }

    private static void configureJavadocJarTask(Project project) {
        def javadoc = project.tasks.findByName('javadoc') as Javadoc
        project.tasks.create('javadocJar', Jar) {
            dependsOn: 'javadoc'
            archiveClassifier.set('javadoc')
            from javadoc.destinationDir
        }
    }
}
