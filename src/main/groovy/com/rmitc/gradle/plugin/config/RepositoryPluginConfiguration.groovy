package com.rmitc.gradle.plugin.config

import org.gradle.api.Project

class RepositoryPluginConfiguration implements PluginConfiguration<Project> {

    @Override
    void configure(Project project) {
        project.repositories.with {
            mavenLocal()
            jcenter()
            mavenCentral()
            maven {
                url  'https://dl.bintray.com/tsiminiya/rmitc'
            }
        }
    }

}
