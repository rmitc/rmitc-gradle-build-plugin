package com.rmitc.gradle.plugin.config


import com.jfrog.bintray.gradle.BintrayPlugin
import com.jfrog.bintray.gradle.tasks.BintrayUploadTask
import com.rmitc.gradle.plugin.extension.RmitcPluginExtension
import org.gradle.BuildAdapter
import org.gradle.api.Project
import org.gradle.api.invocation.Gradle

class BintrayPluginConfiguration implements PluginConfiguration<Project> {

    @Override
    void configure(Project project) {
        project.pluginManager.apply(BintrayPlugin)

        def bintrayUploadTask = project.tasks.findByName(BintrayUploadTask.TASK_NAME) as BintrayUploadTask
        def rmitcExtension = project.extensions.findByType(RmitcPluginExtension)

        project.gradle.addBuildListener([
                bintrayUpload: bintrayUploadTask,
                projectsEvaluated: { Gradle gradle ->
                    bintrayUploadTask.with {
                        publications = ['RMITCLibrary']
                        user = project.properties.jfrogUser
                        apiKey = project.properties.jfrogKey
                        configurations = ['archives']
                        publish = true
                        userOrg = project.properties.jfrogUser
                        repoName = rmitcExtension.bintrayRepoId
                        packageName = rmitcExtension.artifactId
                        packageVcsUrl = rmitcExtension.vcsUrl
                        packageLicenses = ['Apache-2.0']
                        packageLabels = rmitcExtension.labels
                        packagePublicDownloadNumbers = true
                        versionName = project.version
                    }
                }
        ] as BuildAdapter)

        bintrayUploadTask.dependsOn('projectInfo', 'assemble')
    }

}
