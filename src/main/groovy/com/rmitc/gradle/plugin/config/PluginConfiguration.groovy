package com.rmitc.gradle.plugin.config

interface PluginConfiguration<T> {

    void configure(T project)

}
