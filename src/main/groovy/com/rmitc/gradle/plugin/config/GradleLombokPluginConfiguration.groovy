package com.rmitc.gradle.plugin.config

import io.franzbecker.gradle.lombok.LombokPlugin
import org.gradle.api.Project

class GradleLombokPluginConfiguration implements PluginConfiguration<Project> {

    @Override
    void configure(Project project) {
        project.pluginManager.apply(LombokPlugin)
    }

}
