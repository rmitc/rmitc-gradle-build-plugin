package com.rmitc.gradle.plugin

import com.rmitc.gradle.plugin.config.*
import org.gradle.api.Plugin
import org.gradle.api.Project

import static java.util.stream.Collectors.joining

class MainPlugin implements Plugin<Project>, PluginConfiguration<Project> {

    def pluginConfigurations = [
            RepositoryPluginConfiguration,
            BasicPluginConfiguration,
            CompilePluginConfiguration,
            ReleasePluginConfiguration,
            MavenPublishPluginConfiguration,
            GradleLombokPluginConfiguration,
            BintrayPluginConfiguration
    ]

    @Override
    void apply(Project project) {
        configure(project)
    }

    @Override
    void configure(Project project) {
        pluginConfigurations.stream()
                .map { type -> type.getDeclaredConstructor().newInstance() }
                .forEach{ pluginConfiguration -> pluginConfiguration.configure(project) }
    }

}
