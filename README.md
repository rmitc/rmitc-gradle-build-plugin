# RM ITC Gradle Build Plugin

This is a compilation of the following Gradle Plugins:
* Java Plugin
* Groovy Plugin
* Maven Publish
* Gradle Release
* Bintray Artifactory Upload

## Sample Usage

### settings.gradle
```
pluginManagement {
    repositories {
        jcenter()
        mavenLocal()
        mavenCentral()
        gradlePluginPortal()
        maven {
            url  "https://dl.bintray.com/tsiminiya/rmitc"
        }
    }
}

rootProject.name = 'rmitc-app-commons'
```

### build.gradle
```groovy
plugins {
    id 'com.rmitc.build' version '1.0.0'
}

apply plugin: 'com.rmitc.build'

version version

rmitc {
    bintrayRepoId = 'rmitc'
    artifactId = 'rmitc-app-commons'
    vcsUrl = 'https://gitlab.com/rmitc/rmitc-app-commons'
    labels = ['rmitc', 'commons']
}

dependencies {
    compile 'javax.validation:validation-api:2.0.1.Final'
    testCompile 'net.bytebuddy:byte-buddy:1.9.12'
    testCompile 'org.spockframework:spock-core:1.3-groovy-2.5'
}
```

### Versions
* __1.0.0__
First Release: Includes plugins like Lombok, Maven Publish, Bintray Upload to RMITC repository and Release plugin.

* __0.0.7__
Test Version
